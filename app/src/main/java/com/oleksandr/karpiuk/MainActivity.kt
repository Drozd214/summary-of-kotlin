package com.oleksandr.karpiuk

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.oleksandr.karpiuk.challenge.FirstChallenge

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val firstChallenge: FirstChallenge = FirstChallenge()
        firstChallenge.doChallenge()


        val sum = sumValue(5, 10)

    }


    private fun sumValue(x: Int, y: Int) =
        x + y



    private fun showToast(message: String) =
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}
