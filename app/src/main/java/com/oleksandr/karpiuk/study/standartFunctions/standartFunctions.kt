package com.oleksandr.karpiuk.study.standartFunctions

import java.io.File

class standartFunctions {


    //повертає об'єкт з ініціалізованими зміними (receiver)
    //використовується для встановлення об'єкту певних значень
    //зазвичай використовується тоді, коли метод має повернути об'єкт змінивши певні його атрибути
    fun apply() {
        val menuFile = File("menu-file.txt").apply {
            setReadable(true) // Implicitly, menuFile.setReadable(true)
            setWritable(true) // Implicitly, menuFile.setWritable(true)
            setExecutable(false) // Implicitly, menuFile.setExecutable(false)
        }
    }


    //дає доступ до об'єкта через it. Якщо об'єкт нульовий, повертає null. Повертає лямбда-вираз, тобто останній рядок
    //використовується для спроби зробити певні дії з об'єктом та отримати певний результат
    fun let() {
        val firstItemSquared = listOf(1,2,3).first().let {
            it * it
        }

        fun formatGreeting(vipGuest: String?): String {
            return vipGuest?.let {
                "Welcome, $it. Please, go straight back - your table is ready."
            } ?: "Welcome to the tavern. You'll be seated soon."
        }
    }


    //та ж специфіка, що й apply (тобто доступ до полів об'єкта),
    //але повертає не значення на себе, а лямбду
    //звертатись через this
    //використовується для наочності виклику методів
    fun run() {
        fun nameIsLong(name: String) = name.length >= 20
        fun playerCreateMessage(nameTooLong: Boolean): String {
            return if (nameTooLong) {
                "Name is too long. Please choose another name."
            } else {
                "Welcome, adventurer"
            }
        }

        //цей варіант замість нижнього є більш наочним
        "Polarcubis, Supreme Master of NyetHack"
            .run { this.length >= 20 }
            .run(::playerCreateMessage)
            .run { println(this) }

        //приклад без використовування run
        println(playerCreateMessage(nameIsLong("Polarcubis, Supreme Master of NyetHack")))
    }


    //аналог run. Вимагає передати об'єкт явно як параметр
    //це як зовнішній метод, який приймає параметр і запускає лямбду
    //використовується для забрання дублювання назви об'єкта чи класу
    //дозволяє відразу звертатись до полів
    fun with() {
        val nameTooLong = with("Polarcubis, Supreme Master of NyetHack") {
            this.length >= 20
        }
    }


    //те ж, що й let
    //відмінність в тому, що оскільки also повертає сам себе, то можна робити своєрідний ланцюг
    //сама лямбда не повертає нічого, тому використовується лише для додаткових дій з об'єктом
    fun also() {
        var fileContents: List<String>
        File("file.txt")
            .also {
                print(it.name)
            }.also {
                fileContents = it.readLines()
            }
    }


    //працює як вкладений if
    //якщо лямда вертає true - takeIf повертає посилання на себе і можна продовжувати роботу
    //якщо false - takeIf поверне null
    //тому після нього ставимо ?, і якщо повернеться null - всі подальні методи не викличуться
    //використовується, щоб забрати звичайний if чи проміжні дані
    fun takeIf() {
        val fileContents = File("myfile.txt")
            .takeIf { it.canRead() && it.canWrite() }
            ?.readText()
    }


    //takeUnless працює як і takeIf, але повертає null при true-умові в лямбді
    fun takeUnless() {
        val fileContents = File("myfile.txt").takeUnless { it.isHidden }?.readText()
    }


}