package com.oleksandr.karpiuk.study.collections

class Sets {


    //set схожий до звичайного list майже у всьому
    //основна його відмінність - всі його елементи є унікальними і не повторюються
    fun sets(){
        //оскільки останні 2 елементи є однакові, то set просто викине один із них
        val planets = setOf("Mercury", "Earth", "Venus", "Earth")

        //set не містить індексування (list[2]), тому доступатись потрібно до елементів через elementAt
        planets.elementAt(2)
    }


}