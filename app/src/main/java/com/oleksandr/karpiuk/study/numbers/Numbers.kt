package com.oleksandr.karpiuk.study.numbers

import kotlin.math.roundToInt

class Numbers {


    // типи для відображення чисел:
    // Name      Bits    Max Value               Min Value
    // Byte      8       127                     -128
    // Short     16      32767                   -32768
    // Int       32      2147483647              -2147483648
    // Long      64      9223372036854775807     -9223372036854775808
    // Float     32      3.4028235E38            1.4E-45
    // Double    64      1.7976931348623157E308  4.9E-324


    //приклад оголошення числових змінних
    val price: Int = 12345678
    var newProce: Double = 5.91


    // є наступні методи конвертування чисел
    // toFloat
    // toDouble
    // toDoubleOrNull
    // toIntOrNull
    // toLong
    // toBigDecimal


    // toIntOrNull поверне null, оскільки тут є дробова частина, а Елвіс-оператор запише 0
    fun convetringStringToNumeric() {
        val gold: Int = "5.91".toIntOrNull() ?: 0
    }


    //форматування Double за допомогою специфікацій (як в С)
    fun performPurchase(price: Double) {
        // конвертування Int в Double
        var playerGold = 10
        var playerSilver = 45
        val totalPurse = playerGold + (playerSilver / 100.0)

        val remainingBalance = totalPurse - price
        println("Remaining balance: ${"%.2f".format(remainingBalance)}")

        // конвертування Double в Int. roundToInt - округляє до найближчого цілого
        playerGold = remainingBalance.toInt()
        playerSilver = (remainingBalance % 1 * 100).roundToInt()
    }


    fun bitManipulation() {
        val value = 42

        //повертає рядок у двійковому форматі
        Integer.toBinaryString(value)

        //зсув (shift) на 2 біта вліво (left) та вправо (right) відповідно
        value.shl(2)
        value.shr(2)

        //інверсія бітів
        value.inv()

        //відповідні побітові операції
        value.xor(33)
        value.and(33)
        value.or(33)
    }


}