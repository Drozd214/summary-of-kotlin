package com.oleksandr.karpiuk.study.anonymousFunctions

import android.util.Log

class anonymousFunctions {


    //анонімна функція (або лямбда) - це блок коду (як якийсь метод)
    //його особливість в тому, що його не треба описувати як метод
    //а можна написати як параметр іншого методу або як ініціалізація змінної
    //лямбда як і if - повертає останній рядок свого блоку


    //приклад ініціалізації змінної за допомогою лямбда-виразу
    private fun initVariableWithAnonymousFunction() {
        val someValue: () -> String = {
            val year = 2020
            "It is $year year"
        }

        val otherValue = {
            val value = 56
            "Hello, my little boy"
        }
    }


    //те ж саме, що метод someValue(String)
    private fun anonymousFunctionWithParameters(testValue: String = "Oleksandr") {
        val someValue: (String) -> String = { name ->
            "Hello $name"
        }

        someValue("Oleksandr")
    }


    //якщо лямбда приймає лише 1 параметр - його назву можна опустити і звертатись через it
    private fun itKeyword() {
        val someValue: (String) -> String = {
            "Hello $it"
        }
    }


    //якщо параметрів декілька - кожному потрібно вказати назву і тип
    private fun multipleParameters() {
        val someValue: (String, Int) -> String = {name: String, year: Int ->
            "Hello $name, now is $year year"
        }
    }


    //якщо лямбду записати в змінну - її можна потім викликати як окремий метод
    fun doSmth() {
        //приклад використання лямбда-виразу
        val greetingFunction = { playerName: String, numBuildings: Int ->
            val currentYear = 2018
            println("Adding $numBuildings houses")
            "Welcome to SimVillage, $playerName! (copyright $currentYear)"
        }

        //приймає параметр playerName та лямбду, блок якої описаний вище
        fun runSimulation(playerName: String, greetingFunction: (String, Int) -> String) {
            val numBuildings = (1..3).shuffled().last() // Randomly selects 1, 2, or 3
            println(greetingFunction(playerName, numBuildings))
        }
    }


    //також замість лямбди параметром може виступати інший метод
    //виклик його відбувається через посилання на нього за допомогою ::
    fun functionReference() {
        //приймає 1 звичайний параметр та 2 лямбди
        fun runSimulation(playerName: String,
                                 costPrinter: (Int) -> Unit,
                                 greetingFunction: (String, Int) -> String) {
            val numBuildings = (1..3).shuffled().last() // Randomly selects 1, 2, or 3

            //виклик обох лямбд
            costPrinter(numBuildings)
            println(greetingFunction(playerName, numBuildings))
        }

        fun printConstructionCost(numBuildings: Int) {
            val cost = 500
            println("construction cost: ${cost * numBuildings}")
        }

        //при виклику методу, який параметром містить лямбду потрібно врахувати наступне:
        //1) виклик самої лямбди описаний в блоку методу, а не при його виклику
        //2) якщо останнім параметром є лямбда - її можна винести у фігурні дужки ніби псевдо-тіло методу
        //3) при виклику методу, що містить лямбду - ми вказуємо параметри лямбди (через які будемо звертати) та описуємо її тіло
        runSimulation("Guyal", ::printConstructionCost) { playerName, numBuildings ->
            val currentYear = 2018
            println("Adding $numBuildings houses")
            "Welcome to SimVillage, $playerName! (copyright $currentYear)"
        }
    }


    //також метод може повертати лямбду, а не лише приймати її як параметр
    //дуже рідко використовується
    fun functionReturnType() {
        fun configureGreetingFunction(): (String) -> String {
            val structureType = "hospitals"
            var numBuildings = 5
            return { playerName: String ->
                val currentYear = 2018
                numBuildings += 1
                println("Adding $numBuildings $structureType")
                "Welcome to SimVillage, $playerName! (copyright $currentYear)"
            }
        }

        fun runSimulation() {
            val greetingFunction = configureGreetingFunction()
            println(greetingFunction("Guyal"))
        }
    }


    //власна спроба написати лямбду
    //лямбда приймає як параметри ті змінні, які доступні або з тіла методу, де вона викликається
    //або атрибути класи, до яких вона має доступ
    fun ownLambda() {
        fun someFunction(name: String,
                         age: Int,
                         lambda: (String, Int) -> String) {
            var text = "Hello world"

            text += lambda(name, age)
            println(text)
        }

        val name = "Oleksandr"
        val age = 21

        someFunction(name, age) {nameLambda: String, ageLambda: Int ->
            "\nMy name is $nameLambda and I'm $ageLambda years old"
        }
    }


}