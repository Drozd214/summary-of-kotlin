package com.oleksandr.karpiuk.study.interfacesAndAbstractClasses

import java.util.*

class Interface {


    //оголошення інтерфейсу
    //інтерфейс відповідає на запитання "що?" але не відповідає "як?"
    //інтерфейс відповідає за модель поведінки
    //використовувати, коли треба доповнити успадкування
    interface Fightable {
        var healthPoints: Int
        val diceCount: Int
        val diceSides: Int
        //значеня за замовчуванням, яке не обов'язково перевизначати у підкласі
        val damageRoll: Int
            get() = (0 until diceCount).map {
                Random().nextInt(diceSides + 1)
            }.sum()

        fun attack(opponent: Fightable): Int
    }

    //клас, який імплементує інтерфейс
    class Player(
        name: String,
        override var healthPoints: Int = 100,
        var isBlessed: Boolean = false,
        private var isImmortal: Boolean
    ) : Fightable {

        //імплементування даних інтерфейсу
        override val diceCount: Int = 3

        override val diceSides: Int = 6

        override fun attack(opponent: Fightable): Int {
            val damageDealt = if (isBlessed) {
                damageRoll * 2
            } else {
                damageRoll
            }
            opponent.healthPoints -= damageDealt
            return damageDealt
        }
    }


}