package com.oleksandr.karpiuk.study.strings

const val TAVERN_NAME = "Taernyl's Folly"

class Strings {


    //якщо потрібно вказати спец-символ, то його вказують у наступному вигляді \', \$, \\, \t, \b і т. д.
    //рядки ніколи не змінюються. Якщо рядок все ж таки var, то просто створюється новий рядок, який замінює данний


    //визначає індекс певного елемента і отримує підрядок з такого-то індексу по такий-то
    private fun placeOrder() {
        val indexOfApostrophe = TAVERN_NAME.indexOf('\'')
        val tavernMaster = TAVERN_NAME.substring(0 until indexOfApostrophe)

        println("Madrigal speaks with $tavernMaster about their order.")
    }


    //повертає список рядків розділених певним символом
    fun doSmth() {
        fun placeOrder1(menuData: String) {
            val data = menuData.split(',')
            val type = data[0]
            val name = data[1]
            val price = data[2]
            val message = "Madrigal buys a $name ($type) for $price."
            println(message)
        }

        placeOrder1("shandy,Dragon's Breath,5.91")
    }


    //деструктуризація - відразу записуємо в змінні
    private fun destructuring() {
        val text = "shandy,Dragon's Breath,5.91"
        val (type, name, price) = text.split(',')
    }


    //замінює символи на вказані
    private fun toDragonSpeak(phrase: String) =
        phrase.replace(Regex("[aeiou]")) {
            when (it.value) {
                "a" -> "4"
                "e" -> "3"
                "i" -> "1"
                "o" -> "0"
                "u" -> "|_|"
                else -> it.value
            }
        }


    //цикд forEach, до елемента звертатись через it
    private fun forEach() {
        "Dragon's Breath".forEach {
            println("$it\n")
        }
    }


}