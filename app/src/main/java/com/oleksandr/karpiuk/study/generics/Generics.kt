package com.oleksandr.karpiuk.study.generics

class Generics {


    //дженерики дозволяють працювати з типами, які ще не відомі компілятору
    //і створювати методи, які можуть працювати з будь-яким типом
    fun definitionGeneric() {
        class LootBox<T>(item: T) {
            var open = false
            private var loot: T = item

            //дженерик метод, який може повернути будь-який тип
            fun fetch(): T? {
                //якщо open == true, то метод поверне посилання на loot
                //інакше поверне null
                return loot.takeIf { open }
            }
        }

        class Fedora(val name: String, val value: Int)
        class Coin(val value: Int)

        fun main(args: Array<String>) {
            //спочатку створюється змінна, яка приймає тип Fedora, а потім тип Coin
            val lootBoxOne: LootBox<Fedora> = LootBox(Fedora("a generic-looking fedora", 15))
            val lootBoxTwo: LootBox<Coin> = LootBox(Coin(15))

            //якщо отримали посилання - виводимо текст (через run маємо доступ до елементів)
            //якщо отримали null, то ? не допустить продовження виклику коду
            lootBoxOne.fetch()?.run { println("$name was open") }
        }
    }


    fun multipleGenericType() {
        class LootBox<T>(item: T) {
            var open = false
            private var loot: T = item
            fun fetch(): T? {
                return loot.takeIf { open }
            }

            fun <R> fetch(lootModFunction: (T) -> R): R? {
                return lootModFunction(loot).takeIf { open }
            }
        }

        class Fedora(val name: String, val value: Int)
        class Coin(val value: Int)

        val lootBoxOne: LootBox<Fedora> = LootBox(Fedora("a generic-looking fedora", 15))
        val coin = lootBoxOne.fetch() {
            Coin(it.value * 3)
        }
        coin?.let { println(it.value) }
    }


    fun genericConstraint() {
        open class Loot(val value: Int)
        class Fedora(val name: String, value: Int) : Loot(value)
        class Coin(value: Int) : Loot(value)

        class LootBox<T : Loot>(item: T) {
            var open = false
            private var loot: T = item
            fun fetch(): T? {
                return loot.takeIf { open }
            }
            fun <R> fetch(lootModFunction: (T) -> R): R? {
                return lootModFunction(loot).takeIf { open }
            }
        }

        //val lootBox: LootBox<Loot> = LootBox(Fedora("a dazzling fuschia fedora", 15))
        //val fedora: Fedora = lootBox.item // Type mismatch - Required: Fedora, Found: Loot
    }


    fun varargAndGet() {
        open class Loot(val value: Int)
        class Fedora(val name: String, value: Int) : Loot(value)
        class Coin(value: Int) : Loot(value)

        class LootBox<T : Loot>(vararg item: T) {
            var open = false
            private var loot: Array<out T> = item

            fun fetch(item: Int): T? {
                return loot[item].takeIf { open }
            }
            fun <R> fetch(item: Int, lootModFunction: (T) -> R): R? {
                return lootModFunction(loot[item]).takeIf { open }
            }

            operator fun get(index: Int): T? = loot[index].takeIf { open }
        }

        val lootBoxOne: LootBox<Fedora> = LootBox(Fedora("a generic-looking fedora", 15),
            Fedora("a dazzling magenta fedora", 25))

        lootBoxOne.open = true
        lootBoxOne.fetch(1)?.run {
            println("You retrieve $name from the box!")
        }
        val coin = lootBoxOne.fetch(0) {
            Coin(it.value * 3)
        }
        coin?.let { println(it.value) }

        val fedora = lootBoxOne[1]
        fedora?.let { println(it.name) }
    }


}