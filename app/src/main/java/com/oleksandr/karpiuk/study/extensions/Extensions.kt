package com.oleksandr.karpiuk.study.extensions

class Extensions {


    //extension - це можливість розшири вже готовий клас, до якого немає доступу
    fun extension() {
        //додавання нового методу addEnthusiasm до вже готового класу String
        fun String.addEnthusiasm(amount: Int = 1) = this + "!".repeat(amount)
        fun main(args: Array<String>) {
            println("Madrigal has left the building".addEnthusiasm())
        }
    }

    //також можна розширювати суперкласи
    fun extensionOnSuperclass() {
        fun String.addEnthusiasm(amount: Int = 1) = this + "!".repeat(amount)

        //для виклику методу знову й знову (повертаємо receiver)
        fun <T> T.easyPrint(): T {
            println(this)
            return this
        }

        fun main(args: Array<String>) {
            println("Madrigal has left the building").easyPrint()
            42.easyPrint()
            //виклик generic-розширення
            "Hello".easyPrint().addEnthusiasm().easyPrint()
        }
    }

    //також можна розширити властивості, наприклад гетери та сетери
    val String.numVowels
        get() = count { "aeiouy".contains(it) }

    //також можна розширювати нульові типи
    fun String?.printWithDefault(default: String) = print(this ?: default)
}