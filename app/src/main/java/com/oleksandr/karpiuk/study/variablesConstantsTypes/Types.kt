package com.oleksandr.karpiuk.study.variablesConstantsTypes

const val MAX_EXPERIENCE: Int = 5000        //оголошення константи, що ніколи не зміниться

class Types {


    val stringValue: String = "happy meal"  //тип для рядків
    val charValue: Char = 'X'               //символ в Unicode
    val booleanValue: Boolean = false       //тип для true/false
    val intValue: Int = 10                  //тип для цілочисельних значень
    val doubleValue: Double = 3.14          //тип для дробових значень

    //ще є типи Float, Long, Short, Byte
    //також різні колекції, такі як List, Set, Map


}