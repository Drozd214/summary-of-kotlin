package com.oleksandr.karpiuk.study.nullSafetyAndExceptions

import java.lang.Exception

class Exceptions {


    //exception - це певний виняток, який говорить, що щось пішло не так
    var swordsJuggling: Int? = null
    val isJugglingProficient = (1..3).shuffled().last() == 3




    //перевірка на IllegalStateException
    fun firstThrow() {
        //метод, який при нульовому значенні викине певний exception
        fun proficiencyCheck(swordsJuggling: Int?) {
            swordsJuggling ?: throw IllegalStateException("Player cannot juggle swords")
        }

        if (isJugglingProficient) {
            swordsJuggling = 2
        }

        //перевірка на нульове значення
        //оскільки змінну ми перевірили, ми точно знаємо, що вона є не нульовою і можемо застосовувати !!
        proficiencyCheck(swordsJuggling)
        swordsJuggling = swordsJuggling!!.plus(1)
        println("You juggle $swordsJuggling swords!")
    }


    //також можна створити свій власний exception, який наслідуватиме один з стандартних
    class UnskilledSwordJugglerException() :
        IllegalStateException("Player cannot juggle swords")
    //викид кастомного Exception
    //    fun proficiencyCheck(swordsJuggling: Int?) {
    //        swordsJuggling ?: throw UnskilledSwordJugglerException()
    //    }


    //для того, щоб відслідковувати різні exceptions використовують блок try/catch
    fun tryCatch() {
        fun proficiencyCheck(swordsJuggling: Int?) {
            swordsJuggling ?: throw UnskilledSwordJugglerException()
        }

        fun firstThrow() {
            if (isJugglingProficient) {
                swordsJuggling = 2
            }

            //виконується якась дія всередині блоку
            //якщо десь виникне який exception - catch-блок буде порівнювати його зі своїми описаними
            //якщо знайде потрібний - виконає блок всередині певного catch і збою програми не буде
            try {
                proficiencyCheck(swordsJuggling)
                swordsJuggling = swordsJuggling!!.plus(1)
            }
            catch(e: Exception) {
                println(e)
            }
            catch(e: UnskilledSwordJugglerException) {
                println(e)
            }
            println("You juggle $swordsJuggling swords!")
        }

        class UnskilledSwordJugglerException() :
            IllegalStateException("Player cannot juggle swords")
    }


}