package com.oleksandr.karpiuk.study.objects

class DataClasses {


    //data-класи використовуються лише для збереження даних

    //потрібні лише якщо будемо використовувати методи toString, equals, copy

    //є 3 правила їх створення
    //перше - має бути первинний конструктор з хоча б 1 параметром
    //друге - у конструкторі всі змінні мають мати val або var
    //третє - не можуть бути abstract, open, sealed чи inner

    //в data-класах краще реалізований метод toString()
    //він показує дані, а не місце в пам'яті

    //також краще реалізований метод equals
    //оскільки він порівнює не посилання об'єктів, а власне їх дані
    data class Coordinate(val x: Int, val y: Int) {
        val isInBounds = x >= 0 && y >= 0
    }

    fun destructuring() {
        val coordinate = Coordinate(1, 0)
        val (x, y) = coordinate
    }

    //також data-класи мають власний метод copy
    //він створює такий самий об'єкт і дозволяє змінити в ньому певні значення
    //val mortalPlayer = player.copy(isImmortal = false)


}