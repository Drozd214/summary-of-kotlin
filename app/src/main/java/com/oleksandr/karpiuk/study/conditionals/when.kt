package com.oleksandr.karpiuk.study.conditionals

class whenConditional {


    //when - аналог switch в інших мовах
    //якщо не перебрані всі умови when - то має бути обов'язково дефолтне значення else
    //наприклад, чисел чи варіацій рядків може бути безліч
    //а вміст enum-об'єктів є чітко визначений
    fun whenExpression() {
        val race = "gnome"

        val faction = when(race) {
            "dwarf" -> "Keepers of the Mines"
            "gnome" -> "Keepers of the Mines"
            "orc" -> "Free people of the Rolling Hills"
            "human" -> "Free people of the Rolling Hills"
            else -> "Unknown kind of player"
        }
    }


}