package com.oleksandr.karpiuk.study.classes

class Initialization {


    fun Constructors() {
        //первинний конструктор
        //змінні можна оголосити відразу у контрукторі, або в самому класі
        //якщо змінна має кастомні гетер чи сетер - її не можна оголосити в первинному конструкторі
        //якщо ж змінна оголошенна відразу у конструкторі, то її потім не потрібно ініціалізовувати (тобто пропадає дублювання)
        //також можна у конструкторах вказувати дефолтні значення
        class Player(
            name: String = "name",
            healthPoints: Int,
            val isBlessed: Boolean,
            private val isImmortal: Boolean
        ) {


            var name = name
                get() = field.capitalize()
                private set(value) {
                    field = value.trim()
                }
            var healthPoints = healthPoints

            //змінні також можна ініціалізувати за допомогою методів
            val someAge = {
                (1..100).shuffled().first()
            }

            //працює лише з стрінгами та об'єктами класів
            lateinit var someValue: String

            //використовується, якщо треба ініціалізувати щось продуктивно-затратне
            //ініціалізація відбувається лише при звертанні до змінної
            val hometown by lazy {
                val a = 5
                val b = 10
                a + b
            }

            //вторинний конструктор
            //працює як альтернативний
            //або викликає первинний з параметрами, або вторинний який викликає первинний
            //в даному випадку, тут потрібно вказати лише ім'я, а інші значення є сталими
            //також вторинний конструктор може мати в собі певну логіку
            constructor(name: String) : this(name,
                healthPoints = 100,
                isBlessed = true,
                isImmortal = false) {
                if(name == "Kar") {
                    healthPoints -= 40
                }
            }

            //блок ініціалізації, який перевіряє значення на коректність
            //повертає лямбди
            //якщо жодна з умов не виконається - викине IllegalArgumentException
            //викликається незалежно від вибору конструктора (тобто все рівно чи первинний, чи вторинний)
            init {
                require(healthPoints > 0) { "healthPoints must be greater than zero." }
                require(name.isNotBlank(), { "Player must have a name." })
            }
        }

        //виклик конструктора
        val player = Player("oleksandr",
            healthPoints = 100,
            isBlessed = true,
            isImmortal = false)

        //виклик вторинного конструктора
        val player2 = Player("oleksandr")
    }


}