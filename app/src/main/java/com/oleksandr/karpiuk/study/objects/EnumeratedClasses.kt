package com.oleksandr.karpiuk.study.objects

class EnumeratedClasses {


    //enum-класи - це класи-перерахунки чогось, наприклад певних констант
    enum class Direction {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }

    //enum-класи також можуть мати конструктори чи внутрішні функції
    //щоб функції відділити від самого переліку потрібно в кінці поставити ;
    enum class Direction1(val coordinate: Coordinate) {
        NORTH(Coordinate(0, -1)),
        EAST(Coordinate(1, 0)),
        SOUTH(Coordinate(0, 1)),
        WEST(Coordinate(-1, 0));

        fun updateCoordinate(playerCoordinate: Coordinate) =
            Coordinate(playerCoordinate.x + coordinate.x,
                playerCoordinate.y + coordinate.y)

    }
    data class Coordinate(val x: Int, val y: Int) {
        val isInBounds = x >= 0 && y >= 0
    }

    //оскільки виклик йде власне елемента переліку, а не самого класу, то треба звертатись через елемент
    //Direction.EAST.updateCoordinate(Coordinate(1, 0))


}