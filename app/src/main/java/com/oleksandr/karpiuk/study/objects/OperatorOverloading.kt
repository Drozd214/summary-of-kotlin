package com.oleksandr.karpiuk.study.objects

class OperatorOverloading {


    //перевантаження операторів потрібно, якщо ми хочемо працювати з об'єктами через різні операції
    //деякі популярні оператори для цього:
    //+, +=, ==, >, [], .., in
    enum class Direction(private val coordinate: Coordinate) {
        NORTH(Coordinate(0, -1)),
        EAST(Coordinate(1, 0)),
        SOUTH(Coordinate(0, 1)),
        WEST(Coordinate(-1, 0));

        //приклад використання перевантаженого оператора
        fun updateCoordinate(playerCoordinate: Coordinate) =
            coordinate + playerCoordinate
    }

    data class Coordinate(val x: Int, val y: Int) {
        val isInBounds = x >= 0 && y >= 0

        //приклад перевантаження оператора
        operator fun plus(other: Coordinate) = Coordinate(x + other.x, y + other.y)
    }


}