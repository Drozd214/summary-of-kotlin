package com.oleksandr.karpiuk.study.classes

class Inheritance {


    fun inheritance() {
        //open означає, що клас може мати підклас (наслідників)
        open class Room(val name: String) {
            //позначає змінну, до якої є доступ в підкласі
            protected open val dangerLevel = 5

            fun description() = "Room: $name\n" +
                    "Danger level: $dangerLevel"

            //open перед методом означає, що цей метод можна перезаписати
            open fun load() = "Nothing much to see here..."
        }


        open class TownSquare : Room("Town Square") {
            //super - ключове слова для посилання на батьківський клас
            override val dangerLevel = super.dangerLevel - 3
            private var bellSound = "GWONG"

            //метод, який перезаписує батьківський
            final override fun load() = "The villagers rally and cheer as you enter!\n${ringBell()}"

            //власний метод
            private fun ringBell() = "The bell tower announces your arrival. $bellSound"
        }

        //існує наступний нюанс - ми хочемо створити ще підклас підкласу
        //в такому варіанті ми зможемо перезаписати методи, які дозволені для перезапису в батьківському класі
        //якщо ми хочемо це заборонити, то потрібно використовувати ключове слово final
        class testClass : TownSquare() {
            //тут буде помилка, оскільки перезаписаний метод load має ключове слово final
            //тобто його не можливо перезаписати
            //            override fun load(): String {
            //                return super.load()
            //            }
        }

        //хоча currentRoom і є типу Room, об'єкт створений саме з підкласу TownSquare
        //при виклику перезаписаних методів - будуть викликатись перезаписані
        //це приклад поліморфізму, коли об'єкт має тип батьківського класу
        //але, фактично, містить в собі об'єкт підкласу з всіма його перезаписаними та індивідуальними методами та даними
        val currentRoom: Room = TownSquare()
        println(currentRoom.description())
        println(currentRoom.load())

        //ключове слово is дозволяє перевірити, чи об'єкт є екземпляром певного класу
        //оскільки, TownSquare наслідується від Room, то обидві умови будуть повертати true
        val townSquare = TownSquare()
        var className = when(townSquare) {
            is TownSquare -> "TownSquare"
            is Room -> "Room"
            else -> throw IllegalArgumentException()
        }

        //також у котліні існує приведення до типу
        //спочатку відбувається перевірка чи об'єкт є екземпляром TownSquare
        //якщо ні - він приводиться до типу Room
        fun printIsSourceOfBlessings(any: Any) {
            //приклад так званого "розумного кастування"
            //якщо пройшла перевірка, чи тип об'єкта є TownSquare
            //то котлін автоматично розуміє, що це тип TownSquare і звертається до нього відповідно
            val isSourceOfBlessings = if (any is TownSquare) {
                any.load()
            } else {
                (any as Room).load()
            }
        }
    }


}