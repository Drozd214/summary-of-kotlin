package com.oleksandr.karpiuk.study.interfacesAndAbstractClasses

import java.util.*

abstract class AbstractClass {


    interface Fightable {
        var healthPoints: Int
        val diceCount: Int
        val diceSides: Int
        val damageRoll: Int
            get() = (0 until diceCount).map {
                Random().nextInt(diceSides + 1)
            }.sum()

        fun attack(opponent: Fightable): Int
    }

    //абстрактний клас, який імплементовує інтерфейс
    //клас може імплементовувати лише 1 абстрактний клас
    //але може імплементовувати безліч інтерфейсів
    abstract class Monster(val name: String,
                           val description: String,
                           override var healthPoints: Int) : Fightable {

        override fun attack(opponent: Fightable): Int {
            val damageDealt = damageRoll
            opponent.healthPoints -= damageDealt
            return damageDealt
        }
    }


    //клас, який імплементовує абстрактний клас Monsters
    class Goblin(name: String = "Goblin",
                 description: String = "A nasty-looking goblin",
                 healthPoints: Int = 30) : Monster(name, description, healthPoints) {

        override val diceCount: Int = 2
        override val diceSides: Int = 8
    }


}