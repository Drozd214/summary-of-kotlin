package com.oleksandr.karpiuk.study.objects

import java.io.File

class Object {


    //ключове слово object відповідає у котліні за патерн singleton
    //object - клас, який створюється миттєво 1 раз і доступний потім у програмі завжди і будь-де
    object Game {
        private val name: String = "Madrigal"

        init {
            println("Welcome, adventurer.")
        }

        fun play() {
            while (true) {
                // Play NyetHack
            }
        }
    }

    //object-вираз
    //використовується для створення об'єкту певного класу
    //якщо оголошений в тому ж класі - створюється миттєво
    //якщо в іншому - при ініціалізації відбовідного класу
    val abandonedTownSquare = object : TownSquare() {
        override fun load() = "You anticipate applause, but no one is here..."
    }
    open class TownSquare(){
        open fun load() : String {
           return "Room is empty"
        }
    }

    //companion object можна оголосити всередині якогось класу
    //може мати або не мати назву
    //до його методів можна звернутись як до константних методів відразу
    //він створюється у 2 випадках
    //перший - ініціалцізація класу, в якому він оголошений
    //другий - доступ до будь-якого його елемента
    //оскільки це все ж таки object, то до нього є доступ звідусіль
    class PremadeWorldMap {
        companion object {
            private const val MAPS_FILEPATH = "nyethack.maps"
            fun load() = File(MAPS_FILEPATH).readBytes()
        }

        val value = load()
    }


}