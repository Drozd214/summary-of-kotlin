package com.oleksandr.karpiuk.study.nullSafetyAndExceptions

class Preconditions {


    //preconditions - це різні методи, що викидають відповідні exceptions
    //застосовуються вкрай рідко і залежно від своєї ролі

    //є 5 варіантів Preconditions
    //checkNotNull
    //require
    //requireNotNull
    //error
    //assert

    var swordsJuggling: Int? = null
    val isJugglingProficient = (1..3).shuffled().last() == 3




    //перевірка на IllegalStateException
    fun firstThrow() {
        fun proficiencyCheck(swordsJuggling: Int?) {
            //перевіряє чи значення є нульовим і викидає відповідний exception
            checkNotNull(swordsJuggling, { "Player cannot juggle swords" })
        }

        if (isJugglingProficient) {
            swordsJuggling = 2
        }

        proficiencyCheck(swordsJuggling)
        swordsJuggling = swordsJuggling!!.plus(1)
        println("You juggle $swordsJuggling swords!")
    }


}