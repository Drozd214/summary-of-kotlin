package com.oleksandr.karpiuk.study.collections

class LoopsAndArrays {


    //цикл while хороший для того, якщо нам потрібно щось циклічно повторити певну кількість разів
    fun whileLoop() {
        var orderCount = 0

        while (orderCount <= 9) {
            if(orderCount == 5) {
                //оператор break виходить з циклу
                break
            } else {
                //do something
            }
            orderCount++
        }
    }


    fun colectionsConvertion() {
        //конвертування list до set для видалення неунікальних елементів та повернення назад до list
        val patrons = listOf("Eli Baggins", "Eli Baggins", "Eli Ironfoot")
            .toSet()
            .toList()

        //виконує те саме, що й вище. Це спеціальна функція від Kotlin
        val patrons1 = listOf("Eli Baggins", "Eli Baggins", "Eli Ironfoot").distinct()
    }


    //приклад оголошення масиву. Масиви можуть підтримувати різні типи
    fun array() {
        val playerAges: IntArray = intArrayOf(34, 27, 14, 52, 101)
        println(playerAges)
    }


}