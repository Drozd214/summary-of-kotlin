package com.oleksandr.karpiuk.study.conditionals

import android.util.Log

class IfElse {


    val tag = "IfElseTag"

    val name = "Madrigal"
    var healthPoints = 100
    val isBlessed = true
    val isImmortal = false




    //якщо умова виконується - виконується тіло if
    //якщо ні - виконується тіло блоку else, якщо він наявний
    fun simpleIf() {
        if(healthPoints == 100) {
            Log.d(tag, "$name is excellent condition!")
        } else {
            Log.d(tag, "$name is awful condition!")
        }
        //можливі оператори порівнювання:
        //<, <=, >, >=, ==, !=, === (the same reference), !==
    }


    fun nestedIf() {
        if(healthPoints == 100) {
            Log.d(tag, "$name is excellent condition!")
        } else if(healthPoints >= 90) {
            //також один if може бути вкладений в інший
            if(isBlessed) {
                Log.d(tag, "has some minor wounds but is healing quite quickly")
            } else {
                Log.d(tag, "has some minor wounds")
            }
        } else {
            Log.d(tag, "$name is awful condition!")
        }
    }


    //умова може бути складеною, яка поєднується за допомогою логічних операторів
    //бувають такі логічні оператори:
    //&&, ||, ! (and, or, not)
    fun logicOperators() {
        if(isBlessed && healthPoints > 50 || isImmortal) {
            Log.d(tag, "GREEN")
        } else {
            Log.d(tag, "NONE")
        }
    }


    //також за допомогою логічних операторів можна ініціалізовувати змінні
    fun logicalOperatorsInVariablesDeclaration() {
        val auraVisible = isBlessed && healthPoints > 50 || isImmortal
    }


    //if повертає останній рядок, що дозволяє йому ініціалізовувати змінні
    fun conditionalExpression() {
        val auraColor = if(isBlessed && healthPoints > 50 || isImmortal) {
            "GREEN"
        } else {
            "RED"
        }
    }


    //через .. можна оголошувати певні діапазони значень
    fun ranges() {
        val healthStatus = if(healthPoints == 100) {
            "is in excellent condition!"
        } else if(healthPoints in 90..99) {
            "has a few scratches."
        } else if(healthPoints in 15..89) {
            "Looks preety hurt."
        } else {
            "is in awful condition!"
        }
    }


}