package com.oleksandr.karpiuk.study.collections

class Maps {


    //map також може бути лише для читанням та змінним
    //це список, який складається з пар: ключ-значення
    //ключі унікальні, значення можуть дублюватись
    val patronGold = mapOf("Eli" to 10.5, "Mordoc" to 8.0, "Sophie" to 5.5)

    //варіанти доступу до елементів
    val Mordoc = patronGold["Mordoc"]
    val Sophie = patronGold.getValue("Sophie")




    //якщо додати в map пару, ключ якої вже існує, то нове значення замінить старе
    fun maps() {
        println(mapOf("Eli" to 10.75,
            "Mordoc" to 8.25,
            "Sophie" to 5.50,
            "Sophie" to 6.25))
        //{Eli=10.5, Mordoc=8.0, Sophie=6.25}
    }


}