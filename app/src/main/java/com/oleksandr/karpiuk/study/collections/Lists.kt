package com.oleksandr.karpiuk.study.collections

class Lists {


    //список елементів, значення можуть повторюватись
    //якщо список оголошений через listOf - його елементи не можна змінювати
    fun listOf() {
        val patronList: List<String> = listOf("Eli", "Mordoc", "Sophie")

        //доступ до 1 елемента списку
        patronList.first()
        patronList[0]

        //намагається доступитись до вказаного елемента(4), якщо він за межами - повертає лямбду
        patronList.getOrElse(4) { "Unknown element" }

        //те ж, що й getOrElse але вертає не лямбду, а null
        val fifthPatron = patronList.getOrNull(4) ?: "Unknown Patron"

        //перевірка, чи список містить певний елемент
        if (patronList.contains("Eli")) {
            println("The tavern master says: Eli's in the back playing cards.")
        } else {
            println("The tavern master says: Eli isn't here.")
        }

        //перевірка на декілька елементів одночасно
        if (patronList.containsAll(listOf("Sophie", "Mordoc"))) {
            println("The tavern master says: Yea, they're seated by the stew kettle.")
        } else {
            println("The tavern master says: Nay, they departed hours ago.")
        }
    }


    //mutableListOf створює список, який можна редагувати
    fun mutableListOf() {
        val patronListM = mutableListOf("Eli", "Mordoc", "Sophie")

        //видалення елемента та додавання нового (в кінець)
        patronListM.remove("Eli")
        patronListM.add("Alex")

        //додавання елемента за вказаним індексом
        patronListM.add(0, "Alex")

        //змінює тип з mutable на звичайний
        //також можна навпаки через toMutableList
        val readOnlyPatronList = patronListM.toList()
    }


    //для отримання даних зі списку використовують цикли (ітерацію)
    fun iteration() {
        val patronList: List<String> = listOf("Eli", "Mordoc", "Sophie")

        //проходження списку за допомогою циклу for
        for(patron in patronList) {
            println("Good morning $patron")
        }

        //проходження списку за допомогою циклу forEach
        patronList.forEach { patron ->
            println("Good evening, $patron")
        }

        //використання forEachIndexed для отримання індексу поточного елементу
        patronList.forEachIndexed { index, patron ->
            println("Good evening, $patron - you're #${index + 1} in line.")
        }

        //отримання індексу через for
        for((index, item) in patronList.withIndex()) { println("...") }
    }


}