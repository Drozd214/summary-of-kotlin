package com.oleksandr.karpiuk.study.classes

class Classes {


    //клас складається з атрибутів (значень, як вік, ріст, тощо)
    //та поведінки - методи класу, які виконують якісь дії

    //існує 4 модифікатори доступу до елементів класу
    //(та й до всіх методів і змінних також)

    //public (default) - доступ є з будь-якої частини коду у всіх
    //private - доступ є лише в межах видимості {} (наприклад лише в класі)
    //protected - доступ є в межах класу та у дочірніх класів (підкласів)
    //internal - доступ є в тому ж модулі (package)

    fun creation() {
        //оголошення класу
        class Player() {
            //метод класу
            fun castFireball(numFireballs: Int = 2) =
                println("A glass of Fireball springs into existence. (x$numFireballs)")
        }

        //створення екземпляру (об'єкта класу) та виклик його методів
        val player = Player()
        player.castFireball()
    }


    //котлін надає автоматичні гетери та сетери, але їх можна зробити кастомними
    //якщо є потреба зробити доступ до змінної, але треба заборонити її зміну - можна створити приватний сетер
    fun classProperties() {
        class Player {
            //створення кастомних гетера та сетера
            var name = "madrigal"
                get() = name.capitalize()
                set(value) {
                    field = value.capitalize()
                }
        }

        val player = Player()
        val playerName = player.name
        player.name = "oleksandr"
    }


    //обичслювальні атрибути
    //якщо змінна має гетер, який кожен раз вираховується, то так званого поля для змінної не створюється
    //і кожен раз при доступі до змінної ми будемо отримувати її нове значення
    fun computedProperties() {
        class Dice() {
            val rolledValue
                get() = (1..6).shuffled().first()
        }

        val value = Dice()
        value.rolledValue
    }


}