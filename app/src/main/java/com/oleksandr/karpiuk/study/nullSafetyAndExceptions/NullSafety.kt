package com.oleksandr.karpiuk.study.nullSafetyAndExceptions

class NullSafety {


    //котлін слідкує за нульовими значеннями і в нього є 2 варіанти типів
    //наприклад ненульовий звичайний Int
    //або той, що може містити значення null - Int?
    //name може приймати null, age - ні
    var name: String? = "Oleksandr"
    var age: Int = 25




    private fun setNullability() {
        name = null
        //якщо спробувати age присвоїти null - буде помилка, тому що Int != Int?
        //age = null
    }


    fun nullSafety(){
        //оскільки readLine() може повернути null - використовуємо оператор ? щоб сказати, що ми знаємо, що там може повернутись null
        //оператор ? не дозволить далі продовжувати дію над об'єктом, якщо він міститиме null-значення
        val safeCallOperator = readLine()?.capitalize()

        //якщо можливий нульовий тип - можна використовувати let
        var beverage = readLine()?.let {
            if (it.isNotBlank()) {
                it.capitalize()
            } else {
                "Buttered Ale"
            }
        }

        //оператор !! вказує на те, що тут точно не нульове значення
        //якщо там все ж таки нульове - буде NullPointerException
        val doubleBanf = readLine()!!.capitalize()

        //ще один варіант - перевірити через if чи операнд не є нульовим
        var beverage2 = readLine()
        if (beverage2 != null) {
            beverage2 = beverage2.capitalize()
        } else {
            println("I can't do that without crashing - beverage was null!")
        }

        //Elvis-оператор ?: - якщо зліва щось нульове - виконуємо те, що справа
        val beverageServed: String = beverage ?: "Buttered Ale"
    }


}