package com.oleksandr.karpiuk.study.objects

class NesstedClasses {


    //приклад створення вкладеного класу
    object Game {
        private class GameInput(arg: String?) {
            private val input = arg ?: ""
            val command = input.split(" ")[0]
            val argument = input.split(" ").getOrElse(1, { "" })
        }
    }


}