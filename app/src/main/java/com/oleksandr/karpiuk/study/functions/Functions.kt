package com.oleksandr.karpiuk.study.functions

import android.util.Log

class Functions {


    val healthPoints = 50
    val isBlessed = false

    //виклик методу
    val healthStatus = formatHealthStatus(healthPoints, isBlessed)
    val healtStatus2 = formatHealthStatus2(healthPoints, isBlessed)

    //можливий варіант виклику методу, коли певному параметру присвоюється значення
    //є зручним, оскільки відразу видно який параметр які дані отримує
    //в такому випадку можна перевати параметри довільно, я не в порядку
    val healthStatus3 = formatHealthStatus2(healthPoints = healthPoints,
                                            isBlessed = isBlessed)

    //викликик перевантаженої функції
    val healthStatus4 = formatHealthStatus2(isBlessed)




    //звичайна функція, приймає параметри та повертає якесь значення
    private fun formatHealthStatus(healthPoints: Int, isBlessed: Boolean) : String {
        val healthStatus = when (healthPoints) {
            100 -> "is in excellent condition!"
            in 90..99 -> "has a few scratches."
            in 75..89 -> if (isBlessed) {
                "has some minor wounds but is healing quite quickly!"
            } else {
                "has some minor wounds."
            }
            in 15..74 -> "looks pretty hurt."
            else -> "is in awful condition!"
        }

        return healthStatus
    }


    //в методі можна задавати дефолтні значення, щоб потім їх не перевадати як параметри
    //виклик може бути як defaultArguments(1) так і defaultArguments(3, 7).
    //в першому випадку someValue = 5, otherValue = 1
    //в другому - 3 і 7 відповідно
    private fun defaultArguments(someValue: Int = 5, otherValue: Int) {
        Log.d("functions", "someValue = $someValue, otherValue = $otherValue")
    }


    //метод, який містить в собі лише 1 вираз (тобто повертає його і можна вказати такий синтаксис)
    private fun formatHealthStatus2(healthPoints: Int, isBlessed: Boolean) =
        when (healthPoints) {
            100 -> "is in excellent condition!"
            in 90..99 -> "has a few scratches."
            in 75..89 -> if (isBlessed) {
                "has some minor wounds, but is healing quite quickly!"
            } else {
                "has some minor wounds."
            }
            in 15..74 -> "looks pretty hurt."
            else -> "is in awful condition!"
        }


    //приклад перевантаженого методу, який має ту ж назву, але інші параметри
    //також такий метод може мати іншу реалізацію
    private fun formatHealthStatus2(isBlessed: Boolean) =
        when (healthPoints) {
            100 -> "is in excellent condition!"
            in 90..99 -> "has a few scratches."
            in 75..89 -> "has some minor wounds, but is healing quite quickly!"
                .takeIf { isBlessed } ?: "has some minor wounds."
            in 15..74 -> "looks pretty hurt."
            else -> "is in awful condition!"
        }


}