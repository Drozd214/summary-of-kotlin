package com.oleksandr.karpiuk.study.variablesConstantsTypes

class Variables {


    val playerName: String = "Estragon"     //змінна лише для читання, не можна змінити значення
    var experiencePoints: Int = 5           //змінна, значення якої можна змінити




    fun doSmth() {
        experiencePoints = 10
        //playerName = "Madrigal"           //помилка, значення не можна змінити
    }


}