package com.oleksandr.karpiuk.challenge

import android.util.Log

class FirstChallenge {

    val namePlayer = "Estragon"
    val hasSteed = false

    val pubName = "Unicorn's Horn"
    val namePublicanOnDuty = "Nobody"

    val goldValue = 50

    fun doChallenge() {
        val tag = "First Challenge"

        Log.d(tag, "$namePlayer has $goldValue gold and visited $pubName pub")

        val reversedName = namePlayer.reversed()
        Log.d(tag, reversedName)

    }
}